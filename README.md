# Cross the Road
A game made in PyGame as an assignment for ISS

# Requirements
- Python 3
- PyGame

# How to run the game
Clone this repository, install dependencies using `pip install -r requirements.txt` and run the game using `python game.py`

### Note - It is recommended to use virtual environments in python to install dependendecies.

# Game Rules
- Two players play against each other
- Player 1 moves using ARROW Keys and Player 2 moves using the WASD Keys
- The game ends when both the player die
- If both the player are alive and both of them reach the other end the time taken to cross is considered
- If either one of the two players is dead then 5 points are awarded if player crosses fixed obstacle and 10 points are awarded for crossing moving obstacle
- Once both the player are dead the one with higher score wins

# Screenshots
<img src="https://gitlab.com/ishaan.n.shah/cross-the-road/-/raw/3b9d108784c3cc169f087b883ce99405a6240612/screenshots/1.png" alt="Screenshot 1">
<img src="https://gitlab.com/ishaan.n.shah/cross-the-road/-/raw/3b9d108784c3cc169f087b883ce99405a6240612/screenshots/2.png" alt="Screenshot 2">

# Have Fun :tada:
